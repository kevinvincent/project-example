#! /usr/bin/python
# -*- coding:utf-8 -*-

import flask
import requests
import pika
from flask import url_for
from config import APP_KEY
app = flask.Flask(__name__)


@app.route('/item/<name>', methods=['GET'])
def item(name):
        path = "https://svcs.ebay.com/services/search/FindingService/v1?" \
               "SECURITY-APPNAME=" + APP_KEY + ""\
               "&OPERATION-NAME=findItemsByKeywords" \
               "&SERVICE-VERSION=1.0.0" \
               "&RESPONSE-DATA-FORMAT=JSON" \
               "&REST-PAYLOAD" \
               "&keywords=%s" \
               "&paginationInput.entriesPerPage=6&" \
               "GLOBAL-ID=EBAY-FR" \
               "&siteid=71" % name
        r = requests.get(path)
        return r.content

@app.route('/', methods=['GET'])
def home():
    # TODO Find another solution that's not great !
    url_for('static', filename='*')
    return flask.render_template('index.html')


@app.route('/save', methods=['POST'])
def save():
    data = flask.request.data
    connection = pika.BlockingConnection(pika.ConnectionParameters(
        host='localhost'))
    channel = connection.channel()

    channel.queue_declare(queue='mongodb')

    channel.basic_publish(exchange='',
                          routing_key='mongodb',
                          body=data)
    connection.close()
    return flask.Response({"status" : "success"})


if __name__ == '__main__':
    app.run(debug=True)
