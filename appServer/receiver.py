import pika
from pymongo import MongoClient
import simplejson as json


connection = pika.BlockingConnection(pika.ConnectionParameters(
        host='localhost', port=5672))
channel = connection.channel()

channel.queue_declare(queue='mongodb')


def callback(ch, method, properties, body):
    client = MongoClient('localhost', 27017)
    db = client['database_item']
    items_collection = db['items-collection']
    body = json.loads(body)
    for item in body['data']:
        items_collection.update_one({'itemId': item['itemId']}, {'$set' : item}, upsert=True)
    print "body was saved in the mongoDB database"

channel.basic_consume(callback,
                      queue='mongodb',
                      no_ack=True)
channel.start_consuming()