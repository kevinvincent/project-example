from celery import Celery
from celery.schedules import crontab
import smtplib
from pymongo import MongoClient, ASCENDING
from config import MAIL, PASSWORD

celery = Celery("task",  broker='redis://localhost:6379/0')


app = Celery()

@app.on_after_configure.connect
def setup_periodic_tasks(sender, **kwargs):
    sender.add_periodic_task(15.0, send_mail.s(), name='add every 15')


@app.task
def send_mail():
    print("Sending the mail...")
    aggregate = [
        {"$group": {"_id": "$primaryCategory.categoryName"}}
    ]
    client = MongoClient('localhost', 27017)
    db = client['database_item']
    items_collection = db['items-collection']
    # TODO you can do this by using only one mongodb request
    categories = items_collection.aggregate(pipeline=aggregate)
    msg = ""
    for category in categories:
        try:
            msg += "\n\n" + category['_id'][0][0] + "\n"
            items = items_collection.find({"primaryCategory.categoryName": category['_id'][0][0]},
                                          sort=[('sellingStatus.currentPrice.__value__', ASCENDING)])
            cheapest = items[:2]
            line1 = "name:" + cheapest[0]['title'][0] + " price :" + cheapest[0]['sellingStatus'][0]['currentPrice'][0][
                '__value__'] + "EUR \n"
            line2 = "name:" + cheapest[1]['title'][0] + " price :" + cheapest[1]['sellingStatus'][0]['currentPrice'][0][
                '__value__'] + "EUR \n"
            msg += line1
            msg += line2
        except IndexError as e:
            print("json malformed")
    server = smtplib.SMTP('smtp.gmail.com', 587)
    server.starttls()
    server.login(MAIL, PASSWORD)
    server.sendmail(MAIL, MAIL, msg.encode('utf-8').strip())
    server.quit()