
app.controller('BlankAppController', ['$scope', '$http', '$timeout', function($scope, $http, $timeout) {

  $scope.items = [];
  var saveResult = function() {
    $http({
      method: 'POST',
      url: "/save",
      data: { 'data' : $scope.items }
    }).then(function(response) {
        console.log("success")
    }, function errorCallback(response) {
        console.log("error in save api")
    });
  }

  var call = function() {
    $http({
      method: 'GET',
      url: "/item/" + $scope.itemName
    }).then(function(response) {
      $scope.items = response.data.findItemsByKeywordsResponse[0].searchResult[0].item
      $timeout(call, 8000);
      saveResult()
      console.log("success")
    }, function errorCallback(response) {
        console.log("error in call api")
    });
  }

  $scope.submit = function() {
    call()
  }
}]);