app.directive('ebayItem', function() {
  return {
      restrict: 'E',
      replace: 'true',
      scope: {
         item: '='
      },
      templateUrl: '/static/components/ebayItemComponent/ebayItemTemplate.html'
  };
});